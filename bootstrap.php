<?php
use R3C\Wordpress\EventDispatcher\EventDispatcher;

EventDispatcher::addListener(function() {
    add_action( 'wp_enqueue_scripts', 'r3cWordpressSemanticScripts' );
    add_action( 'admin_enqueue_scripts', 'r3cWordpressSemanticScripts' );
});

function r3cWordpressSemanticScripts() {
    $url = str_replace('/wp', '/', get_site_url());
    $css = $url . 'wp-content/plugins/wordpress-semantic/css/';
    $js = $url . 'wp-content/plugins/wordpress-semantic/javascript/';

	if ( current_user_can('manage_options')) {
		wp_enqueue_script('semantic', $js . 'semantic.min.js', array(), '0.19.3');
        wp_enqueue_style('semantic', $css . 'semantic.min.css', array(), '0.19.3');
	}
}

?>